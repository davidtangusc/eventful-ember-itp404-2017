import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    let eventID = params.id;
    let events = this.modelFor('search');
    // console.log(events);

    return events.find(function(event) {
      return event.id === eventID;
    });
  }
});
