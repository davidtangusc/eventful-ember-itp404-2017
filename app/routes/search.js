import Ember from 'ember';
import $ from 'jquery';

export default Ember.Route.extend({
  model(params) {
    // console.log(params.keywords);
    let keywords = params.keywords;
    let url = `https://api-eventful.herokuapp.com/api/events?keywords=${keywords}`;
    return $.getJSON(url);
  }
});
