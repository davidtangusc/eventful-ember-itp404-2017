import Ember from 'ember';
import moment from 'moment';

export default Ember.Controller.extend({
  sortedEvents: Ember.computed('model', function() {
    let events = this.get('model');

    let sortedEvents = events.sort(function(eventA, eventB) {
      let aUnix = moment(eventA.start_time);
      let bUnix = moment(eventB.start_time);
      if (aUnix < bUnix) {
        return -1;
      }
      if (aUnix > bUnix) {
        return 1;
      }
      return 0;
    });

    return sortedEvents;
  })
});

