import Ember from 'ember';

export default Ember.Controller.extend({
  favorites: Ember.inject.service(),
  isFavorited: Ember.computed('model', 'favorites.data', function() {
    return this.get('favorites.data').includes(this.get('model'));
    // is this.get('model') in this.get('favorites.data')
  }),
  actions: {
    favorite(newIsFavoritedValue) {
      // this.set('isFavorited', newIsFavoritedValue);
      let eventfulEvent = this.get('model');
      if (newIsFavoritedValue) {
        this.get('favorites').add(eventfulEvent);
      } else {
        this.get('favorites').remove(eventfulEvent);
      }
    }
  }
});
