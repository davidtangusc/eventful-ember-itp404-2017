import Ember from 'ember';

export default Ember.Controller.extend({
  isFavorited: true,
  // dont do this
  // isFavoritedChange: Ember.observer('isFavorited', function() {
  //   console.log('changed');
  // })
  actions: {
    setName(name) {
      this.set('name', name);
      // do something else
    },
    // setName(event) {
    //   this.set('name', event.target.value);
    //   // console.log(event);
    // },
    favorite(newIsFavoritedValue) {
      // ajax calls
      this.set('isFavorited', newIsFavoritedValue);
    }
  }
});
