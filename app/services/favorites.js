import Ember from 'ember';

export default Ember.Service.extend({
  data: [],
  add(eventfulEvent) {
    this.set('data', this.data.concat([ eventfulEvent ]));
    // console.log(this.data);
    // $.ajax({
    //   type: 'POST',
    //   url: 'http://localhost:3000/api/favorites',
    //   data: {
    //     event_id: eventfulEvent.id,
    //     title: eventfulEvent.title
    //   }
    // });
  },
  remove(eventfulEvent) {
    let newListOfEvents = this.data.filter((event) => {
        return event !== eventfulEvent;
    });

    this.set('data', newListOfEvents);
    // AJAX DELETE
    // console.log(this.data);
  }
});
