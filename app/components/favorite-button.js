import Ember from 'ember';

export default Ember.Component.extend({
  click() {
    // this.set('favorited', !this.get('favorited'));
    // this.toggleProperty('favorited');
    let favoritedToggled = !this.get('favorited');
    this.get('onclick')(favoritedToggled);
    // console.log(this.get('favorited'));
  }
});
