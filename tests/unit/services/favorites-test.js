import { moduleFor, test, skip } from 'ember-qunit';

moduleFor('service:favorites', 'Unit | Service | favorites', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

test('adding an event to our favorites when nothing has been added', function(assert) {
  // Arrange
  let favorites = this.subject();
  let event = { id: 1, name: 'Some event name' };

  // Act
  favorites.add(event);

  // Assert
  assert.deepEqual(favorites.get('data'), [event]);
});


test('removing an event from our favorites', function(assert) {
  let eventA = { id: 1, name: 'Some event name' };
  let eventB = { id: 2, name: 'Another event name' };

  let favorites = this.subject({
    data: [eventA, eventB]
  });

  favorites.remove(eventB);

  assert.deepEqual(favorites.get('data'), [eventA]);
});

// TODO
skip('adding an event that has already been added');
skip('removing an event that is not favorited');
