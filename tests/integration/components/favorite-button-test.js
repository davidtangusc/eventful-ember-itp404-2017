import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('favorite-button', 'Integration | Component | favorite button', {
  integration: true
});

test('it renders a filled heart when favorited is true', function(assert) {
  this.set('isFavorited', true);
  this.set('handleClick', function() {});
  this.render(hbs`
    {{favorite-button favorited=isFavorited onclick=handleClick}}
  `);
  assert.equal(this.$('.fa-heart').length, 1);
  assert.equal(this.$('.fa-heart-o').length, 0);
});

test('it renders an empty heart when favorited is false', function(assert) {
  this.set('isFavorited', false);
  this.set('handleClick', function() {});
  this.render(hbs`
    {{favorite-button favorited=isFavorited onclick=handleClick}}
  `);
  assert.equal(this.$('.fa-heart').length, 0);
  assert.equal(this.$('.fa-heart-o').length, 1);
});

test('onclick is invoked with the opposite value of favorited', function(assert) {
  this.set('isFavorited', false);
  this.set('handleClick', function(newIsFavorited) {
    assert.equal(newIsFavorited, true);
  });
  this.render(hbs`
    {{favorite-button favorited=isFavorited onclick=handleClick}}
  `);
  this.$('i').click();
});
