import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('eventful-event', 'Integration | Component | eventful event', {
  integration: true
});

test('it renders the city name, country, and start date', function(assert) {
  this.set('eventfulEvent', {
    start_time: '2017-12-22 19:00:00',
    city_name: 'Los Angeles',
    country_name: 'USA'
  });
  this.render(hbs`{{eventful-event event=eventfulEvent}}`);
  assert.equal(this.$().text().trim(), 'December 22nd, 2017 - Los Angeles, USA');
});
