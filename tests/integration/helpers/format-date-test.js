import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('format-date', 'helper:format-date', {
  integration: true
});

test('it formats a date in the format of 2017-12-22 19:00:00', function(assert) {
  // Arrange
  this.set('start', '2017-12-22 19:00:00');

  // Act
  this.render(hbs`{{format-date start}}`);

  // Assert
  assert.equal(this.$().text().trim(), 'December 22nd, 2017');
});

