import { test } from 'qunit';
import moduleForAcceptance from 'eventful/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | search events');

test('searching for events', function(assert) {
  visit('/');
  fillIn('#search-input', 'machine gun kelly');
  click('#search-button');

  andThen(function() {
    assert.equal(currentURL(), '/search/machine%20gun%20kelly', 'search term was put in the URL');
    assert.equal(find('[data-test="event"]').length, 3, 'events were rendered on the page');
  });
});
